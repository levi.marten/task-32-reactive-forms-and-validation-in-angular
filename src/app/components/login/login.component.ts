import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() loginAttempt: EventEmitter<any> = new EventEmitter;

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(2), Validators.maxLength(64), Validators.email ]),
    password: new FormControl('', [ Validators.required ])
  });

  private loginResult: any = {
    isLoggedIn: false,
    message: ''
  };

  isLoading: boolean = false;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username');
  };

  get password() {
    return this.loginForm.get('password');
  };

  async onLoginClicked() {
    try {
      this.isLoading = true;

      const result: any = await this.auth.login(this.loginForm.value);

    } catch (e) {
      alert('Error: ' + e.error.error);

    } finally {
      this.isLoading = false;
    };
  };
};
