import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Output() signUpAttempt: EventEmitter<any> = new EventEmitter;

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(2), Validators.maxLength(64), Validators.email ]),
    password: new FormControl('', [ Validators.required, Validators.minLength(12) ])
  });

  isLoading: boolean = false;

  private signUpResult: any = {
    isSignedUp: false,
    message: ''
  };

  constructor(private auth: AuthService) { }

  get username() {
    return this.registerForm.get('username');
  };

  get password() {
    return this.registerForm.get('password');
  };

  ngOnInit(): void {
  }

  async onSignUpClicked() {
    try {
      this.isLoading = true;

      const result: any = await this.auth.register(this.registerForm.value);

    } catch (e) {
      alert('Error: ' + e.error.error);

    } finally {
      this.isLoading = false;
    }
  }

}
